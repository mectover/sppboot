package com.example.demo.Topic;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.stereotype.Service;

@Service
public class TopicService {
	
	
	private List<Topic> topics=new ArrayList<>(Arrays.asList(
			new Topic("1","spring","framework"),
			new Topic("2","spring2","Core java"),
			new Topic("3","Angular","Javascript")
			));
	
	public List<Topic> getAllTopics(){
		
		return topics;
	}
	
	public Topic getTopic(String id) {
			
			Topic vv = null;
			for(Topic v : topics) {
				
						if(v.getId().equals(id))  vv=v;
								  }
		return vv;
	}
	
	public void addTopic(Topic t) {
		
				this.topics.add(t);
	}

}
